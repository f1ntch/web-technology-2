'use strict';
const data = require("../data/formula1");
let {drivers} = data;

const findById = (id) => {
    return drivers.filter(el => el.id === parseInt(id))
};


module.exports = findById;


