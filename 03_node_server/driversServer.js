'use strict';
const http = require('http');
const getDriverWithTeam = require('./driversService');
const data = require("../data/formula1");
const poort = 3001;


const requestHandler = (req, res) => {
    let driverId = /[^/]*$/.exec(req.url)[0];

    if (req.url === '/') {
        res.writeHead(400, {'Content-Type': 'text/plain'});
        res.end("Geef een id mee in de URL.")

    } else if (isNaN(driverId)) {
        res.writeHead(400, {'Content-Type': 'text/plain'})
        res.end("Er is geen geldig id in de URL gevonden.")

    } else if (parseInt(driverId) > data.drivers.length) {
        res.writeHead(404, {'Content-Type': 'text/plain'})
        res.end(`Driver met ID: ${driverId} bestaat niet.`)

    } else {
        res.writeHead(200, {'Content-Type': 'application/json'})
        res.end(JSON.stringify(getDriverWithTeam(parseInt(driverId))));
    }


    console.log("URL: " + req.url)
};


const server = http.createServer(requestHandler);


server.listen(poort, () => {
    console.log(`Server gestart op poort ${poort}\n http://localhost:${poort}`)
})
