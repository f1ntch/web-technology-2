'use strict';
const data = require("../data/formula1");
let {teams} = data;


const findBy = (subTest) => {
    return teams.filter(subTest) // subTest krijgt lambda mee vanuit driverService.js
}


module.exports = findBy;
