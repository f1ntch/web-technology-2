'use strict';
const findById = require('./driversRepository');
const findBy = require('./teamsRepository');

const getDriverWithTeam = (driverId) => {
    // findById haalt driver array op via id
    let driver = findById(driverId);
    // findBy looped over teams array via lambda en voegt deze toe aan de array driver
    driver[0].team = findBy(el => el.driverId === driverId)
    return driver[0];
};


module.exports = getDriverWithTeam;
