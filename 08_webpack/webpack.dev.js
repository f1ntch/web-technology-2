const path = require('path');
const common = require('./webpack.common');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = merge(common, {
    mode: "development",
    devServer: {
        contentBase: "./dist",
        open: true,
        overlay: true,
        compress: true,
    }

});
