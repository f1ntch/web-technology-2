"use strict";

/** Drivers GET ALL */
//http://localhost:3000/api/drivers
const getAllDrivers = async (driversUrl) => {
    const data = await fetch(driversUrl, {
        method: 'GET'
    });
    return await data.json();
};

/** Drivers PUT */
//http://localhost:3000/api/drivers/
const putDriver = async (driverUrl, driver) => {
    const result = await fetch(driverUrl, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(driver)
    });
    return result.status;
};

/** Drivers DELETE by ID*/
//http://localhost:3000/api/drivers/id
const deleteSpecificDriver = async (specificDriverUrl) => {
    return fetch(specificDriverUrl, {
        method: 'DELETE'
    });
};

/** Drivers GET by ID */
//http://localhost:3000/api/drivers/1
const selectSpecificDriver = async (specificDriverUrl) => {
    const result = await fetch(specificDriverUrl, {
        method: 'GET'
    });

    return await result.json();
};

/** Drivers POST */
//http://localhost:3000/api/drivers/
const postNewDriver = async (driverUrl, driver) => {
    const result = await fetch(driverUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(driver)
    });
    return result.status;
};

/** Teams GET by ID */
//http://localhost:3000/api/drivers/1/teams
const getAllTeams = async (teamUrl) => {
    const result = await fetch(teamUrl, {
        method: 'GET'
    });
    return await result.json();
};

export {getAllDrivers, deleteSpecificDriver, selectSpecificDriver, postNewDriver, getAllTeams,putDriver};
