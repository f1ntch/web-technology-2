"use strict";
import {
    fillDriverList,
    showNewForm,
    addNew,
    showStart,
    searchDriver,
    editDriver,
    showDrivers,
    showDriversDetails
} from "./lijstPanel.js";


document.getElementById("drivers").addEventListener('click', showDrivers);
document.getElementById("driverDetails").addEventListener('click', showDriversDetails);

/** Event listeners*/

document.getElementById('new').addEventListener('click', showNewForm);
document.getElementById('cancel').addEventListener('click', showStart);
document.getElementById('cancel-edit').addEventListener('click', showStart);

document.getElementById('add').addEventListener('click', addNew);


document.getElementById('edit').addEventListener('click', editDriver);
document.getElementById('search').addEventListener('click', searchDriver);
document.getElementById("driverstable").addEventListener('click', showStart);








