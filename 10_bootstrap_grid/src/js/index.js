"use strict";
import {fillDriverList, showNewForm, addNew, showStart, searchDriver, editDriver} from "./lijstPanel.js";

/** Event listeners*/
// window.addEventListener('load', fillDriverList);
document.getElementById('new').addEventListener('click', showNewForm);
document.getElementById('cancel').addEventListener('click', showStart);
document.getElementById('back').addEventListener('click', fillDriverList);
document.getElementById('add').addEventListener('click', addNew);
document.getElementById('edit').addEventListener('click', editDriver);
document.getElementById('search').addEventListener('click', searchDriver);
document.getElementById("driverstable").addEventListener('click', showStart);

