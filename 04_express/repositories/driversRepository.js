'use strict';
const data = require("../../data/formula1");
let {drivers} = data;
const addSelf = require("../util/hal");
const path = "/api/drivers/";

const findById = (id) => {
    return drivers.filter(el => el.id === parseInt(id))
};

const getDriversBasicInfo = (req) => {
    return drivers.map(el => {
        let {id, firstName, lastName} = el;
        return {id, firstName, lastName, _links: addSelf(el,req,path)}
    })


};

module.exports = {findById, getDriversBasicInfo};



