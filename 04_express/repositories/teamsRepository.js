'use strict';
const data = require("../../data/formula1");
let {teams} = data;
const addSelf = require("../util/hal");
const path = "/api/teams/";

const findBy = (subTest,req) => {
    return teams.filter(subTest).map(el => {
        let {id, teamName, country} = el;
        return {id, teamName, country, _links: addSelf(el, req, path)}

    })
};

const getTeamsBasicInfo = (req) => {
    return teams.map(el => {
        let {id, teamName, country} = el;
        return {id, teamName, country, _links: addSelf(el, req, path)}
    })
};

module.exports = {findBy, getTeamsBasicInfo};
