'use strict';
const teamsRepository = require('./teamsRepository');
const {findBy} = require("./teamsRepository");
const {findById} = require("./driversRepository");

const getTeamOfDriver = (driverId,req) => {
    const driverTeams = teamsRepository.findBy(el => el.driverId === driverId,req)
    return driverTeams;
};



const getDriverWithTeams = (driverId,req) => {
    // findById haalt driver array op via id
    let driver = findById(driverId);
    // findBy looped over teams array via lambda en voegt deze toe aan de array driver
    driver[0].team = findBy(el => el.driverId === driverId,req)
    return driver[0];
};


module.exports = {getTeamOfDriver,getDriverWithTeams};


