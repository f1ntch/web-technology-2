const request = require("supertest");
const app = require("../app");
const url = require("url");


describe("test drivers", () => {
    test("GET all drivers check if status code is 200", async () => {
        let res = await request(app).get("/api/drivers");
        expect(res.statusCode).toBe(200)
    });

    test("GET all drivers check code is 200", async () => {
        let res = await request(app).get("/api/drivers");
        expect(res.body.length).toBeGreaterThan(10);
    });


    test("GET Self_url", async () => {
        let res = await request(app).get("/api/drivers");
        let selflinkId = parseInt(Object.values(res.body[0]._links.self).toString().slice(-1));

        let linkObj = await request(app).get("/api/drivers/1");
        expect(linkObj.body[0].id).toBe(selflinkId)
    });


    test("POST driver check if status code is 201", (done) => {
        let res = request(app).post("/api/drivers");
        res.send({"firstName": "Jael", "lastName": "Romero"}).expect(201, done)

    });


});


describe("Test teams", () => {
    test("GET teams and check if driverId equals the id of driver", async () => {
        let res = await request(app).get("/api/teams/1");
        res.body.team.forEach(el => expect(el.driverId).toEqual(1))


    });
});

