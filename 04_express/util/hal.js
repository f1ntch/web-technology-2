const url = require("url");

const addSelf = (object,req,path) => {
    let href = url.format({
        protocol: req.protocol,
        host: req.get("host"),
        pathname: path + object.id
    });
    return {"self": {"href": href}}
};

module.exports = addSelf;
