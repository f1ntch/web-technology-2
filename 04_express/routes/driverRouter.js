"use strict";
const express = require('express');
const router = express.Router();
const driverRepository = require("../repositories/driversRepository");
let {drivers} = require("../../data/formula1");
const bodyParser = require('body-parser');

router.use(express.json());
router.use(bodyParser.json());


const getDriversInfo = (req, res) => {
    if (Object.keys(req.query).length === 0) {
        res.json(driverRepository.getDriversBasicInfo(req))
    } else {
        const filteredDrivers = driverRepository.getDriversBasicInfo(req).filter(el =>
            el.firstName.toLowerCase().includes(req.query.zoek.toLowerCase()) || el.lastName.toLowerCase().includes(req.query.zoek.toLowerCase())
        );
        if (filteredDrivers.length !== 0) {
            res.json(filteredDrivers)
        } else {
            res.status(404).send("Geen resulaten gevonden..");
        }
    }

};

const getDriverById = (req, res) => {
    res.json(driverRepository.findById(parseInt(req.params.driverId)))

};

const getDriverByQuery = (req, res) => {
    console.log(req.query.zoek)

};


const deleteDriver = (req, res) => {
    const driver = drivers.find(el => el.id === parseInt(req.params.driverId));
    if (!driver) {
        res.status(404).send("Driver not found")
    }

// Haal index op van het gevonden driver object
    const idx = drivers.indexOf(driver);
// Verwijder driver object
    drivers.splice(idx, 1);
    res.send("Driver removed");
};

const postDriver = (req, res) => {

    /**  AANGEPAST VOOR SPA PROJECT */

    if (req.body.firstName.length === 0) {
        res.status(400).send("Firstname mag niet leeg zijn")
        return
    }

    if (req.body.lastName.length === 0) {
        res.status(400).send("Lastname mag niet leeg zijn")
        return
    }

    const driver = {
        id: drivers.length + 1,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        Nationality: req.body.nationality,
        totalGPWins: req.body.gpWins,
        active: req.body.active,
        lastGPWin: req.body.lastGpWin,
        image: req.body.image

    };

    res.status(201).send("Driver toegevoegd")

    drivers.push(driver);

    res.json(driver);


};

function putDriver(req, res) {
    drivers.forEach(function (el) {
        if (el.id === parseInt(req.body.id)) {
            el.firstName = req.body.firstName;
            el.lastName = req.body.lastName;
            el.Nationality = req.body.nationality;
            el.totalGPWins = req.body.gpWins;
            el.active = req.body.active;
            el.lastGPWin = req.body.lastGpWin;
            el.image = req.body.image;
        }
    });


    res.send("Driver aangepast")

}

router.get("/api/drivers/:driverId", getDriverById);
router.get("/api/drivers/", getDriversInfo);

// router.get("/api/drivers/:zoek", getDriverByQuery);

router.delete("/api/drivers/:driverId", deleteDriver);
router.post("/api/drivers/", postDriver);
router.put("/api/drivers/:driverId", putDriver);

module.exports = router;
