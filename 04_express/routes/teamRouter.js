"use strict";
const express = require('express');
const router = express.Router();
const {getTeamOfDriver} = require('../repositories/driversService');
const {getDriverWithTeams} = require("../repositories/driversService");
const {getTeamsBasicInfo} = require("../repositories/teamsRepository");
const {teams} = require("../../data/formula1")
const {drivers} = require("../../data/formula1")


const getTeamsById = (req, res) => {
    res.json(getTeamOfDriver(parseInt(req.params.driverId), req))
};


const getTeams = (req, res) => {
    res.json(getTeamsBasicInfo(req))
};

const getTeamsAndDriver = (req, res) => {
    const team = teams.filter(t => t.driverId === parseInt(req.params.teamId));
    const driver = drivers.filter(t => t.id === parseInt(req.params.teamId));
    const output = {
        team, driver
    };


    res.json(output)

};


router.get("/api/drivers/:driverId/teams", getTeamsById);
router.get("/api/teams", getTeams);
router.get("/api/teams/:teamId", getTeamsAndDriver);
module.exports = router;

