const app = require("./app");
const poort = 3000;

app.listen(poort, () => console.log(`Server gestart op poort ${poort}\n http://localhost:${poort}/api/drivers/`));
