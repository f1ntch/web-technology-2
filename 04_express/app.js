"use strict";
const express = require('express');
const cors = require('cors')
const morgan = require('morgan');
const app = express();
const driverRouter = require("./routes/driverRouter");
const teamRouter = require("./routes/teamRouter");
app.use(cors())
app.use(express.static("public"))

app.use(morgan("dev"));
app.use("/", driverRouter);
app.use("/", teamRouter);

module.exports = app;
