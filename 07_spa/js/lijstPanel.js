//todo add cursors
//todo Refactor code
//todo transitie effect


"use strict";
import {
    getAllDrivers,
    deleteSpecificDriver,
    selectSpecificDriver,
    getAllTeams,
    putDriver,
    postNewDriver
} from "./restClient.js";

/** URL rest server */
const driversUrl = "http://localhost:3000/api/drivers";

/** row-ID*/
let driverId = 0;

/** Tabel elementen */
const driversTable = document.getElementById("drivers-table");
const teamsTable = document.getElementById("teams");

/** Tabel aanmaken en vullen met drivers data en eventlisterns koppelen per rij voor selectie en verwijderen van drivers*/
const fillDriverList = async () => {
    document.getElementById("search-message").classList.add("hidden");
    emptyDriversList();
    const driversList = await getAllDrivers(driversUrl); //JSON inlezen
    await createTable(driversList);

};

const createTable = async (driversList) => {



// Voor elk element in driversList wordt een rij aangemaakt in de tabel
    [...driversList].forEach((el, index, array) => {
        // img element voor vuilbakje wordt aangemaakt
        const trashcan = document.createElement("img");
        trashcan.src = "./assets/remove.svg";

        // Er wordt eerst voor iedere driver een <tr> element aangemaakt dat wordt geappend aan de tabel "drivers-table" en krijgt als id row-driverId + id van de drivers
        driversTable.appendChild(document.createElement("tr")).setAttribute("id", "row-driverId-" + el.id);

        // Het <tr> element wordt aangemaakt via het row-driverId + id van de driver
        let row = document.getElementById("row-driverId-" + el.id);

        // Het id van de driver wordt toegevoegd aan de rij
        row.appendChild(document.createElement("td")).innerHTML = el.id;
        // De voornaam van de driver wordt toegevoegd aan de rij
        row.appendChild(document.createElement("td")).innerHTML = el.firstName;
        // De achternaam van de driver wordt toegevoegd aan de rij
        row.appendChild(document.createElement("td")).innerHTML = el.lastName;

        // De afbeelding van de vuilbakje wordt toegevoegd aan de rij en krijgt als id het nummer van driver en als class img-delete
        row.appendChild(document.createElement("td")).appendChild(trashcan).setAttribute("id", el.id);
        document.getElementById(el.id).setAttribute("class", "img-delete");


        // TABEL heeft 2 OPTIES selecteren of verijderen via vuilbak afbeelding
        if (index === array.length - 1) {
            // Hang achter elk vuilbakje een eventlistner die de driver verwijderd
            [...document.getElementsByClassName("img-delete")].forEach(el => {

                //<<< DELETE FUNCTIE AANROEPEN >>>
                el.addEventListener('click', deleteDriver);
                // Hang achter elk rij van de tabel een eventlistner die de details van de driver weergeeft

                //<<< SELECTEER FUNCTIE AANROEPEN >>>
                el.parentNode.parentNode.parentNode.addEventListener('click', selectDriver);

                // Via parentNode gaan borrelen we op tot de teams-table id's komen

            });
        }

        document.getElementById("back").classList.add("hidden");

    });
};


/** Driver zoeken */
const searchDriver = async () => {

    let inputValue = document.getElementById('search-field').value.toLowerCase();

    if (inputValue === "") {
        await fillDriverList()
        return;
    }

    const driversList = await getAllDrivers(driversUrl); //JSON inlezen
    // gefilterd resultaat op basis van inputvalue
    const searchResult = driversList.filter(el => el.firstName.toLowerCase().includes(inputValue) || el.lastName.toLowerCase().includes(inputValue));

    if (inputValue !== '' && searchResult.length !== 0) {
        emptyDriversList();
        await createTable(searchResult);
        document.getElementById("back").classList.remove("hidden");


    } else {
        document.getElementById("search-message").classList.remove("hidden");
        emptyDriversList();
        document.getElementById("back").classList.remove("hidden");


    }


};

/** Driver toevoegen annuleren*/
const showStart = () => {
    document.querySelector(".details-header").classList.remove("hidden")
    document.getElementById("new").classList.remove("hidden");
    document.getElementById("data-show").classList.remove("hidden");
    document.getElementById("teams").classList.remove("hidden");
    document.getElementById("edit").classList.remove("hidden");
    document.getElementById("new").classList.remove("hidden");
    document.querySelector(".details-new").classList.add("hidden")
    document.getElementById("input-form").classList.add("hidden");
    document.getElementById("cancel").classList.add("hidden");
};


/** Drivers tabel leeg maken*/
const emptyDriversList = () => {
    while (driversTable.hasChildNodes()) {
        // Verwijder alle childs nodes uit de driver tabel
        driversTable.removeChild(driversTable.lastChild);
    }
};

/** Teams tabel leeg maken*/
const emptyTeamsList = () => {
    while (teamsTable.hasChildNodes()) {
        // Verwijder alle childs nodes uit de teams tabel
        teamsTable.removeChild(teamsTable.firstChild);
    }
    document.getElementById("message").innerHTML = "";
};


/** Driver verwijderen*/
const deleteDriver = async (event) => {
    /*id naam ophalen via event dat binnenkomt
    Het id wordt opgehaalt via event.target.id dit is id van <img> dat we hebben aangemaakt via .appendChild(trashcan).setAttribute("id", el.id);

 */
    const id = event.target.id;
    // url  maken om door te gegeven aan AJAX deleteSpecificDriver
    const specificDriverUrl = driversUrl + "/" + id;
    // Alert bericht
    if (confirm(`Ben je zeker dat je driver met ID:${id} wil verwijderen?`)) {
        // wachten op prommise van deleteSpecificDriver
        const response = await deleteSpecificDriver(specificDriverUrl);
        //checken of status = OK
        if (response.status === 200) {
            console.log(`Driver with id ${id} was deleted successfully.`);
            // Tabel leeg maken
            emptyDriversList();
            // Tabel vullen
            await fillDriverList();
        }

    }
    emptyDriversList()
    await fillDriverList();

};


/** Driver selecteren en weergeven met detail informatie en teams*/
const selectDriver = async (event) => {
    document.getElementById("add-edit").classList.add("hidden")
    document.getElementById("new").classList.remove("hidden")
    document.getElementById("edit").classList.remove("hidden")

    const id = event.target.parentNode.id.replace(/[^0-9]/g, '');
    driverId = id;

    const specificDrivernUrl = driversUrl + "/" + id;
    const driver = await selectSpecificDriver(specificDrivernUrl); //JSON inlezen

    console.log(driver);
    console.log("Driver " + id + " geslecteerd")

    document.getElementById("id").innerText = id;
    document.getElementById("select-id").value = id;
    document.getElementById("select-firstname").value = driver[0].firstName;
    document.getElementById("select-lastname").value = driver[0].lastName;
    document.getElementById("select-total-gp-wins").value = driver[0].totalGPWins;
    document.getElementById("select-nationality").value = driver[0].Nationality;
    document.getElementById("select-last-gp-victory").value = driver[0].lastGPWin;
    document.getElementById("driver-image").src = driver[0].image;


    if (driver[0].active) {
        document.getElementById("select-active").innerHTML = "Yes"
    } else {
        document.getElementById("select-active").innerHTML = "No"
    }


    emptyTeamsList();

    await getTeamsFormDriver(id);
};

/**  */
const showNewForm = async () => {

    const list = await getAllDrivers(driversUrl);
    const lastDriverId = list[list.length - 1].id;

    document.getElementById("input-form-id").value = lastDriverId + 1;
    document.getElementById("data-show").classList.add("hidden");
    document.getElementById("teams").classList.add("hidden");
    document.getElementById("new").classList.add("hidden");
    document.querySelector(".details-header").classList.add("hidden");
    document.getElementById("edit").classList.add("hidden");
    document.getElementById("input-form").classList.remove("hidden");
    document.getElementById("cancel").classList.remove("hidden");
    document.getElementById("add").classList.remove("hidden");
    document.querySelector(".details-new").classList.remove("hidden");


};

/** Driver toevoegen */
const addNew = async (event) => {

    emptyTeamsList();

    const firstName = document.getElementById("input-form-firstname").value;
    const lastName = document.getElementById("input-form-lastname").value;
    const nationality = document.getElementById("input-form-nationality").value;
    const gpWins = document.getElementById("input-form-total-gp-wins").value;
    const lastGpWin = document.getElementById("input-form-last-gp-win").value;
    const active = document.querySelector('.input-form-active').checked;
    const image = "https://via.placeholder.com/200x80.png?text=" + firstName;

    const driver = {
        firstName,
        lastName,
        nationality,
        gpWins,
        lastGpWin,
        active,
        image
    };


    // Geef het driver object door aan postDriver  functie
    // verwacht 2 parameters  URL (http://localhost:3000/api/drivers/) , Object

    console.log(driversUrl);

    const status = await postNewDriver(driversUrl, driver);
    if (status === 201) console.log("Driver successfully added.");

    // drivers tabel leeg maken
    emptyDriversList();

    //Tabel vullen
    await fillDriverList();

    //Verbergen van input velden na creatie
    document.getElementById("input-form").classList.add("hidden");
    document.getElementById("add").classList.add("hidden");
    document.getElementById("cancel").classList.add("hidden");
    document.getElementById("data-show").classList.remove("hidden");
    document.getElementById("new").classList.remove("hidden");
    document.getElementById("teams").classList.remove("hidden");
    document.getElementById("add").classList.add("hidden");
    document.getElementById("cancel").classList.add("hidden");

    showStart();


};


/** Driver editeren */

const editDriver = async () => {

    // Toggle classes
    document.getElementById("new").classList.add("hidden");
    document.getElementById("edit").classList.add("hidden");
    document.getElementById("add-edit").classList.remove("hidden")

    document.getElementById("select-firstname").value = "";
    document.getElementById("select-lastname").value = "";
    document.getElementById("select-total-gp-wins").value = "";
    document.getElementById("select-nationality").value = "";
    document.getElementById("select-last-gp-victory").value = "";
    document.getElementById("driver-image").src = "https://via.placeholder.com/200x80.png?text=Edit";

    //Remove readonly
    document.getElementById('select-firstname').removeAttribute('readonly');
    document.getElementById('select-lastname').removeAttribute('readonly');
    document.getElementById('select-total-gp-wins').removeAttribute('readonly');
    document.getElementById('select-nationality').removeAttribute('readonly');
    document.getElementById('select-last-gp-victory').removeAttribute('readonly');


    document.getElementById("add-edit").addEventListener('click', async function () {

        emptyDriversList();
        const id = driverId;
        const firstName = document.getElementById("select-firstname").value;
        const lastName = document.getElementById("select-lastname").value;
        const gpWins = document.getElementById("select-total-gp-wins").value;
        const nationality = document.getElementById("select-nationality").value;
        const lastGpWin = document.getElementById("select-last-gp-victory").value;

        const driver = {
            id,
            firstName,
            lastName,
            nationality,
            gpWins,
            lastGpWin
        };

        let driversUrlpost = driversUrl + "/" + driverId;
        const status = await putDriver(driversUrlpost, driver);
        if (status === 201) console.log("Driver successfully alterd.");
        emptyDriversList();
        await fillDriverList();

    });


};


/** Teams ophalen*/
const getTeamsFormDriver = async (id) => {
    // url klaar maken
    const teamUrl = driversUrl + "/" + id + "/teams";
    console.log("Teams ophalen: " + teamUrl);
    // Functie geeft prommise terug met teams object
    // wachten op prommise van selectSpecificDriver
    const teamsList = await getAllTeams(teamUrl);
    console.log(teamsList);

    if (teamsList.length !== 0) {
        // Doorloop teams en maak een <tr> element aan
        teamsList.forEach((el, index) => {
            // Het <tr> element wordt aangemaakt via het row-team- + index
            document.getElementById("teams").appendChild(document.createElement("tr")).setAttribute("id", "row-team-" + index);

            // Haal het aangemaakte rij element op
            const row = document.getElementById("row-team-" + index);

            // Voeg de team data toe aan de tabel
            row.appendChild(document.createElement("td")).innerHTML = el.id;
            row.appendChild(document.createElement("td")).innerHTML = el.teamName;
            row.appendChild(document.createElement("td")).innerHTML = el.country;
        })
    } else {
        document.getElementById("message").innerHTML = "No teams where found for this driver";
    }
};

export {fillDriverList, deleteDriver, showNewForm, addNew, getTeamsFormDriver, showStart, searchDriver, editDriver};
