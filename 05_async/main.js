const data = require('../data/formula1');
const fsp = require('fs').promises;
const writeTeams = require("./teams");
let {drivers, teams} = data;


fsp.mkdtemp("teams")
    .then(folder => {
        process.chdir(folder);
        console.log(`Temp dir: ${folder} aangemaakt@ ${timestamp()}`);
        return folder;
    })
    .then((folder) => drivers.forEach(driver => writeTeams(driver, teams)))
    .catch((err) => console.log(err));


function timestamp() {
    let date = new Date();
    return `${date.getSeconds()}:${date.getMilliseconds()}`;
}


