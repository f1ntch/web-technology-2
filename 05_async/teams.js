const fsp = require('fs').promises;
const path = require('path');


const writeTeamsAsync = async (driver, teams) => {
    let teamsFiltered = teams.filter(team => team.driverId === driver.id);
    try {
        await fsp.writeFile(path.join(".", `${driver.firstName}_${driver.lastName}.json`), JSON.stringify(teamsFiltered), err => {
            if (err) console.log("Fout bij schrijven: ", err.code);
        })

    } catch (error) {
        console.log(error)
    }

};


module.exports = writeTeamsAsync;
