const fs = require('fs');
const path = require('path');


//  JSON DRIVERS ARRAY[]    JSON TEAMS ARRAY[]
function writeTeams(driver, teams) {
    let teamsFiltered = teams.filter(team => team.driverId === driver.id);
    console.log(teamsFiltered )

    fs.writeFile(path.join(".", `${driver.firstName}_${driver.lastName}.json`), JSON.stringify(teamsFiltered), err => {
        if (err) console.log("Fout bij schrijven: ", err.code);
    })

}
module.exports = writeTeams;
