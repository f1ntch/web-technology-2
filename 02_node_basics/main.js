const data = require('../data/formula1');
const fs = require('fs');
const writeTeams = require("./teams");
let {drivers, teams} = data;



fs.mkdtemp("team", (err, folder) => {
    if (err) console.log("FOUT: ", err);
    else {
        try {
            process.chdir(folder);
            console.log(`Temp dir: ${folder}`);
            drivers.forEach(driver => writeTeams(driver, teams));
        } catch (err) {
            console.log(`chdir: ${err}`);
        }
    }
});


